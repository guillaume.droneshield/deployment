#!/bin/bash

# Make sure only root can run our script
if [[ `id -u` -ne 0 ]] ; then echo "Please run as root" ; exit 1 ; fi

apt update
apt upgrade -y
apt install curl gpg
apt autoremove -y

# Install salt-minion
curl -L https://bootstrap.saltstack.com -o install_salt.sh
sh install_salt.sh -P -x python3


echo "Enter a name for your minion, followed by [ENTER]:"

read MINION


read -p "What is this device used for? [1] for smarthub OR [2] for compute-node followed by [ENTER] " answer
case ${answer:0:1} in
    1 )
        ROLE="smarthub"
        PRE="sh"
    ;;
    2 )
        ROLE="compute-node"
        PRE="cn"
    ;;
    * )
        echo wrong choice
    ;;
esac


hostname-ctl set-hostname $PRE-$MINION

> minion_id
echo "$PRE-$MINION" >> minion_id

> /etc/salt/minion.d/salt.conf
echo "master: saltmaster.droneshield.team" >> /etc/salt/minion.d/salt.conf
echo "grains:" >> /etc/salt/minion.d/salt.conf
echo "  role: $ROLE" >> /etc/salt/minion.d/salt.conf
echo "  deployment: prem" >> /etc/salt/minion.d/salt.conf



service salt-minion restart

# Install zerotier
curl -s 'https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg' | gpg --import && \
if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi

# Join ZT net
zerotier-cli join 83048a06327e66e3